<?php

namespace App\Tests\Unit;

use App\Entity\Client;
use App\Entity\Voiture;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $location = new Location();

        $dateDebut = new \DateTime('2023-01-01 12:00:00');
        $location->setDateDebut($dateDebut);
        $this->assertEquals($dateDebut, $location->getDateDebut());

        $dateRetour = new \DateTime('2023-01-05 12:00:00');
        $location->setDateRetour($dateRetour);
        $this->assertEquals($dateRetour, $location->getDateRetour());

        $location->setPrix(150.0);
        $this->assertEquals(150.0, $location->getPrix());

        $client = new Client();
        $location->setClient($client);
        $this->assertEquals($client, $location->getClient());

        $voiture = new Voiture();
        $location->setVoiture($voiture);
        $this->assertEquals($voiture, $location->getVoiture());
    }
}
