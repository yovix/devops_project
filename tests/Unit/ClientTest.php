<?php

namespace App\Tests\Unit;

use App\Entity\Client;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testGettersAndSetters() : void
    {
        $client = new Client();

        $client->setCin(123456);
        $this->assertEquals(123456, $client->getCin());

        $client->setNom('Nom');
        $this->assertEquals('Nom', $client->getNom());

        $client->setPrenom('Prenom');
        $this->assertEquals('Prenom', $client->getPrenom());

        $client->setAdresse('123, address');
        $this->assertEquals('123, address', $client->getAdresse());
    }

    public function testAddOrRemoveLocationRelationship(): void
    {
        $client = new Client();
        $location = new Location();

        $client->addLocation($location);
        $this->assertTrue($client->getLocation()->contains($location));
        $this->assertEquals($client, $location->getClient());

        $client->removeLocation($location);
        $this->assertFalse($client->getLocation()->contains($location));
        $this->assertNull($location->getClient());
    }
}
