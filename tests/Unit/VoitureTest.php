<?php

namespace App\Tests\Unit;

use App\Entity\Voiture;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $voiture = new Voiture();

        $voiture->setSerie('123ABC');
        $this->assertEquals('123ABC', $voiture->getSerie());

        $dateMiseEnMarche = new \DateTime('2023-01-01');
        $voiture->setDateMiseEnMarche($dateMiseEnMarche);
        $this->assertEquals($dateMiseEnMarche, $voiture->getDateMiseEnMarche());

        $voiture->setModele('Model Y');
        $this->assertEquals('Model Y', $voiture->getModele());

        $voiture->setPrixJour(100.50);
        $this->assertEquals(100.50, $voiture->getPrixJour());
    }

    public function testAddOrRemoveLocationRelationship(): void
    {
        $voiture = new Voiture();
        $location = new Location();

        $voiture->addLocation($location);
        $this->assertTrue($voiture->getLocation()->contains($location));
        $this->assertEquals($voiture, $location->getVoiture());

        $voiture->removeLocation($location);
        $this->assertFalse($voiture->getLocation()->contains($location));
        $this->assertNull($location->getVoiture());
    }
}
