<?php

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureTest extends WebTestCase
{
    /**
     * @group second
     */
    public function testSaveCarFormWithErrors(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/voiture/new');

        $btn = $crawler->selectButton('Save');
        $form = $btn->form([
            'voiture[serie]' => '',
            'voiture[dateMiseEnMarche][month]' => '12',
            'voiture[dateMiseEnMarche][day]' => '12',
            'voiture[dateMiseEnMarche][year]' => '2023', 
            'voiture[modele]' => '',
            'voiture[prixJour]' => '',
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertSelectorTextContains('body', 'Ecrire serie');
        $this->assertSelectorTextContains('body', 'Ecrire modèle');
        $this->assertSelectorTextContains('body', 'Ecrire prix par jour');
    }

    /**
     * @group second
     */
    public function testSaveCarFormAndRedirect(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/voiture/new');

        $btn = $crawler->selectButton('Save');
        $form = $btn->form([
            'voiture[serie]' => '123ABC',
            'voiture[dateMiseEnMarche][month]' => '12',
            'voiture[dateMiseEnMarche][day]' => '12',
            'voiture[dateMiseEnMarche][year]' => '2023', 
            'voiture[modele]' => 'Model Z',
            'voiture[prixJour]' => 220.25,
        ]);
        $client->submit($form);
        
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertRouteSame('app_voiture_index');
    }
}
