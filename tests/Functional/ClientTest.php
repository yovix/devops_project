<?php

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientTest extends WebTestCase
{
    /**
     * @group first
     */
    public function testEmptyClientFormWithErrors(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/client/new');

        $btn = $crawler->selectButton('Save');
        $form = $btn->form([
            'client[nom]' => '',
            'client[prenom]' => '',
            'client[cin]' => '',
            'client[adresse]' => '',
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertSelectorTextContains('body', 'Ecrire votre cin');
        $this->assertSelectorTextContains('body', 'Ecrire votre nom');
        $this->assertSelectorTextContains('body', 'Ecrire votre prenom');
        $this->assertSelectorTextContains('body', 'Ecrire votre adresse');
    }

    /**
     * @group first
     */
    public function testCinClientWithError(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/client/new');

        $btn = $crawler->selectButton('Save');
        $form = $btn->form([
            'client[nom]' => 'nom',
            'client[prenom]' => 'prenom',
            'client[cin]' => 12,
            'client[adresse]' => 'adresse',
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertSelectorTextContains('body', 'CIN doit etre 8 chiffres');
    }

    /**
     * @group first
     */
    public function testSaveClientFormAndRedirect(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/client/new');

        $btn = $crawler->selectButton('Save');
        $form = $btn->form([
            'client[nom]' => 'nom',
            'client[prenom]' => 'prenom',
            'client[cin]' => 12345678,
            'client[adresse]' => 'adresse',
        ]);
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertRouteSame('app_client_index');
    }
}
