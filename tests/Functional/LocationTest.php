<?php

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationTest extends WebTestCase
{
    /**
     * @group third
     */
    public function testCreateLocationWithExistingClientAndCarAndEndDateEarlierThanStartDate(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/location/new');

        $form = $crawler->selectButton('Save')->form([
            'location[dateDebut][time][hour]' => '12',
            'location[dateDebut][time][minute]' => '30',
            'location[dateDebut][date][day]' => '20',
            'location[dateDebut][date][month]' => '2',
            'location[dateDebut][date][year]' => '2023',

            'location[dateRetour][time][hour]' => '12',
            'location[dateRetour][time][minute]' => '30',
            'location[dateRetour][date][day]' => '10',
            'location[dateRetour][date][month]' => '2',
            'location[dateRetour][date][year]' => '2023',

            'location[prix]' => 150.00,
            'location[client]' => 1,
            'location[voiture]' => 1,
        ]);
      
        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertRouteSame('app_location_new');
        $this->assertSelectorTextContains('body', 'la date de fin doit etre > à la date de debut');
    }
}
