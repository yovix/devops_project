<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Voiture;
use App\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dateDebut', DateTimeType::class, [])
            ->add('dateRetour', DateTimeType::class, [
                'constraints' => [
                    new Callback([$this, 'validateDateRange']),
                ],
            ])
            ->add('prix', NumberType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Ecrire votre prix']),
                ],
            ])
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotBlank(['message' => 'Choisir un client']),
                ],
            ])
            ->add('voiture', EntityType::class, [
                'class' => Voiture::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotBlank(['message' => 'Choisir une voiture']),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
        ]);
    }

    public function validateDateRange($value, ExecutionContextInterface $context): void
    {
        $dateDebut = $context->getRoot()['dateDebut']->getData();
        $dateRetour = $value;

        if ($dateDebut instanceof \DateTimeInterface && $dateRetour instanceof \DateTimeInterface) {
            if ($dateRetour <= $dateDebut) {
                $context->buildViolation('la date de fin doit etre > à la date de debut')
                    ->atPath('dateRetour')
                    ->addViolation();
            }
        }
    }
}
