<?php

namespace App\Form;

use App\Entity\Voiture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class VoitureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('serie', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Ecrire serie']),
                ],
            ])
            ->add('dateMiseEnMarche')
            ->add('modele', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Ecrire modèle']),
                ],
            ])
            ->add('prixJour', NumberType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Ecrire prix par jour']),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Voiture::class,
        ]);
    }
}
